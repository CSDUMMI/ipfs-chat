#!/usr/bin/env node
/**
 * @author Joris Gutjahr
 * @license GPL3-or-later
 * A command line chat room service
 * without any server using IPFS PubSub.
 */

const IPFS = require("ipfs")
const inquirer = require("inquirer")

const yargs = require("yargs")

async function main() {
  const ipfs = await IPFS.create()

  yargs.scriptName("ipfs-chat")
       .usage("$0 <cmd> [args]")
       .command("join [name] [room]", "Join a chat room", (yargs) => {
         yargs.positional("room", {
           type: "string",
           default: "ipfs-chat",
           describe: "The chat room to join"
         })
         yargs.positional("name", {
           type: "string",
           default: "anon",
           describe: "Your username"
         })
       }, function(argv) {
         let bottom = new inquirer.ui.BottomBar();

         console.log(`You are joining ${argv.room} as ${argv.name}`)
         ipfs.pubsub.subscribe( argv.room
                              , logMsg(argv.name)
                            );

         console.log(`Joined ${argv.room} as ${argv.name}`);

         (async () => {
           while (true) {
             const options = { type: "input"
                             , name: "message"
                             , message: argv.room
                             }
             const response = await inquirer.prompt([options])

             let message = response.message

             if(message.charAt(0) == "!") {
               await parseCommand(message);
               continue;
             }

             let msg = JSON.stringify({
               author: argv.name,
               content: message
             })
             ipfs.pubsub.publish(argv.room, Buffer.from(msg, "utf-8"))
           }
         })()

       })
       .help()
       .argv
}

async function parseCommand(command) {
  switch (command) {
    case "!exit":
      let topics = await ipfs.pubsub.ls();

      for (let topic of topics) {
        ipfs.pubsub.unsubscribe(topic, console.log)
      }

      process.exit();
      break;
    default:
      console.log("unknown command");
  }
}
function logMsg(username) {
  return raw_msg => {
    let msg = JSON.parse(raw_msg.data.toString("utf-8"))
    if(username == "anon" || username != msg.author) {
      console.log(formatMsg(msg))
    }
  }
}

function formatMsg(msg) {
  return `[${Date.now().toString()}] ${msg.author}> ${msg.content}`;
}

main().then().catch(e => console.error(e))
