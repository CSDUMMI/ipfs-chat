port module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Json.Encode as E
import Message exposing (Message)

main =
  Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- PORTS
port sendMessage : (String, String) -> Cmd msg
port joinTopic : String -> Cmd msg
port messageReceiver : (String -> msg) -> Sub msg

-- MODEL
type alias Model =
  { username : String
  , draft : String
  , room : String
  , messages : List Message
  }

init : () -> (Model, Cmd Msg)
init flags =
  ({ username = "anon"
   , draft = ""
   , room = "ipfs-chat"
   , messages = []
   }
  , joinTopic "ipfs-chat"
  )

-- UPDATE
type Msg
  = ChangeUsername String
  | ChangeDraft String
  | ChangeRoom String
  | Send
  | Recv String

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    ChangeUsername username ->
      ( { model | username = username }
      , Cmd.none
      )
    ChangeDraft draft ->
      ( { model | draft = draft }
      , Cmd.none
      )
    ChangeRoom room ->
      ( { model | room = room }
      , joinTopic room
      )
    Send ->
      ( { model | draft = "" }
      , let message = Message model.username model.draft
            message_json = Message.encode message
            message_str = E.encode 0 message_json
        in sendMessage (message_str, model.room)
      )
    Recv message_str ->
      let message = D.decodeString Message.decoder message_str
          messages = case message of
            Err _ -> model.messages
            Ok message_ -> message_::model.messages
      in ( { model | messages = messages }
         , Cmd.none
         )

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions _ =
  messageReceiver Recv

-- VIEW
view : Model -> Html Msg
view model =
  div []
  [ h1 [] [ text <| "IPFS Chat " ++ model.room ]
  , input
      [ type_ "text"
      , placeholder "Username"
      , onInput ChangeUsername
      , value model.username
      ]
      []
  , input
      [ type_ "text"
      , placeholder "Room"
      , onInput ChangeRoom
      , value model.room
      ]
      []
  , ul []
      (List.map (\msg -> li [] [ text <| Message.toString msg ]) <| List.reverse model.messages)
  , input
      [ type_ "text"
      , placeholder "Type..."
      , onInput ChangeDraft
      , on "keydown" (ifIsEnter Send)
      , value model.draft
      ]
      []
  , button [ onClick Send ] [ text "Send" ]
  ]

ifIsEnter : msg -> D.Decoder msg
ifIsEnter msg =
  D.field "key" D.string
    |> D.andThen (\key -> if key == "Enter" then D.succeed msg else D.fail "some other key")
