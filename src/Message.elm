module Message exposing ( Message
                        , encode
                        , decoder
                        , toString
                        )
import Json.Decode as D
import Json.Encode as E

type alias Message =
  { author : String
  , content : String
  }

encode : Message -> E.Value
encode message =
  E.object
    [ ("author", E.string message.author)
    , ("content", E.string message.content)
    ]

decoder : D.Decoder Message
decoder =
  D.map2 Message
    (D.field "author" D.string)
    (D.field "content" D.string)

toString : Message -> String
toString message = message.author ++ "> " ++ message.content
